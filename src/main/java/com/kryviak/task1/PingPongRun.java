package com.kryviak.task1;

class PingPongRun {
    private NewThreads pingPongThreads = new NewThreads();

    void runThreads() {
        pingPongThreads.t1.start();
        pingPongThreads.t2.start();
        pingPongThreads.t3.start();
    }
}
