package com.kryviak.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class NewThreads extends Thread {
    private static Logger logger = LogManager.getLogger(NewThreads.class);
    private boolean flag = true;
    private final Object obj = new Object();

    Thread t1 = new Thread(() -> {
        while (flag) {
            logger.info("First thread say: Ping");
            synchronized (obj) {
                obj.notify();
            }
        }
    });

    Thread t2 = new Thread(() -> {
        synchronized (obj) {
            while (flag) {
                logger.info("Second thread say: Pong");
                try {
                    obj.wait();
                } catch (InterruptedException e) {
                    logger.error(e);
                }
            }
        }
    });

    Thread t3 = new Thread(() -> {
        synchronized (obj) {
            logger.info("Last thread say: Stop");
            flag = false;
        }
    });
}
