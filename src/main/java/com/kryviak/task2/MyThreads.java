package com.kryviak.task2;

import com.kryviak.task2.generator.Fibonacci;
import com.kryviak.task2.print.PrintNumber;

class MyThreads extends Thread {
    private static final int GENERATE_FIRST_NUMBER_OF_FIBONACCI = 7;
    private static final int GENERATE_SECOND_NUMBER_OF_FIBONACCI = 10;

    private Fibonacci fibonacci = new Fibonacci();
    private PrintNumber printNumber = new PrintNumber();

    @Override
    public void run() {
        setPriority(1);
        printNumber.printFibonacciNumber(fibonacci.generateFibonacciNumber(GENERATE_FIRST_NUMBER_OF_FIBONACCI));
    }

    Thread t2 = new Thread(() -> {
        printNumber.printFibonacciNumber(fibonacci.generateFibonacciNumber(GENERATE_SECOND_NUMBER_OF_FIBONACCI));
    });
}
