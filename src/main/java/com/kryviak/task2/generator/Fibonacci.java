package com.kryviak.task2.generator;

public class Fibonacci {

    public int generateFibonacciNumber(int numberOfFibonacci){
        if(numberOfFibonacci==0) {
            return 0;
        }
        if (numberOfFibonacci==1) {
            return 1;
        }else {
            return generateFibonacciNumber(numberOfFibonacci-1)+generateFibonacciNumber(numberOfFibonacci-2);
        }
    }
}
