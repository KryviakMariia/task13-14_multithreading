package com.kryviak.task2.print;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PrintNumber {
    private static Logger logger = LogManager.getLogger(PrintNumber.class);

    public void printFibonacciNumber(int fibonacciNumber) {
        logger.info("The number of fibonacci is: " + fibonacciNumber);
    }
}
